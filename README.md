# Custom configuration for my terminal + vim
## Whats in here?
__A bash profile, vimrc and a git config that are basically my awesome go to's. Hope you like!__
### Terminal
* A bash profile set up with significant aliases
* Shell prompt colors (with rbenv)

### VIM/MVIM
* A supremely stacked vim config (which works best with MacVim) **Make sure to use have some sort of vim plugin manager. I use Vundler**

### GIT
* All significant aliases
* Pretty colors
