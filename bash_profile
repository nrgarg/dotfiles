alias l="ls -alG"
alias ls="ls -G"
alias be="bundle exec"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias vi="vim"

function parse_git_branch {
ref=$(git symbolic-ref HEAD 2> /dev/null) || return
echo " ["${ref#refs/heads/}"]" # I wanted my branch wrapped in [], use () or <> or whatever
}


#PS1="\h:\W \u\$" # For reference, here's the default OS X prompt

# I had to put the \[ and \] down here, as opposed to $IRED, to avoid wrapping funkiness.
#export PS1="\[$IRED\]\$(rvm_version)\[$NO_COLOR\]\W\[$LCYA\]\$(parse_git_branch)\[$NO_COLOR\] \$ "
#export PATH=/opt/local/bin:/opt/local/sbin:$PATH
#export PATH=/usr/local/bin:/usr/local/sbin:~/bin:$PATH


export PS1="\[\e[0;31m\]\$(rbenv version-name) \[\e[0;35m\](\W)\[\e[0;36m\]\$(parse_git_branch) \$\[\033[0m\] "

if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi
eval "$(rbenv init -)"
export PATH=/usr/local/bin:$PATH

export NVM_DIR="/Users/ngarg/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
