set nocompatible
" Load Vundle. Manages all of the bundles.
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" So Vundle can update itself.
Plugin 'gmarik/Vundle.vim'
Plugin 'PeterRincker/vim-argumentative'
Plugin 'Wolfy87/vim-enmasse'
Plugin 'guns/vim-sexp'
Plugin 'helino/vim-json'
Plugin 'junegunn/vader.vim'
Plugin 'marijnh/tern_for_vim'
Plugin 'mhinz/vim-signify'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'rking/ag.vim'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-fireplace'
Plugin 'tpope/vim-leiningen'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-markdown'
Plugin 'tpope/vim-projectionist'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-unimpaired'
Plugin 'tpope/vim-vinegar'
Plugin 'walm/jshint.vim'
Plugin 'tpope/vim-rails'
Plugin 'vim-ruby/vim-ruby'
Plugin 'tpope/vim-endwise'
Plugin 'tpope/vim-rake'
Plugin 'slim-template/vim-slim'
Plugin 'altercation/vim-colors-solarized.git'
Plugin 'mileszs/ack.vim'
Plugin 'digitaltoad/vim-jade'


" Plugins with settings.
Plugin 'justinmk/vim-sneak'
  let g:sneak#streak = 1


Plugin 'Raimondi/delimitMate'

Plugin 'SirVer/ultisnips'
  let g:UltiSnipsExpandTrigger="<c-j>"
  let g:UltiSnipsJumpForwardTrigger="<c-j>"
  let g:UltiSnipsJumpBackwardTrigger="<c-k>"
  let g:UltiSnipsEditSplit="vertical"
  let g:UltiSnipsSnippetsDir="~/.vim/snippets"

Plugin 'bling/vim-airline'
  let g:airline#extensions#tabline#enabled=1
  let g:airline_left_sep=''
  let g:airline_right_sep=''

Plugin 'edkolev/promptline.vim'
  let g:promptline_powerline_symbols=0
  " Rest of config at bottom so it has access to autoload.

Plugin 'kien/ctrlp.vim'
  let g:ctrlp_user_command='ag %s -l --nocolor -g ""'

Plugin 'kien/rainbow_parentheses.vim'
  let g:rbpt_max = 21
  let g:rbpt_colorpairs = [
    \['blue', '#FF6000'],
    \['cyan', '#00FFFF'],
    \['darkmagenta', '#CC00FF'],
    \['yellow', '#FFFF00'],
    \['red', '#FF0000'],
    \['darkgreen', '#00FF00'],
    \['White', '#c0c0c0'],
    \['blue', '#FF6000'],
    \['cyan', '#00FFFF'],
    \['darkmagenta', '#CC00FF'],
    \['yellow', '#FFFF00'],
    \['red', '#FF0000'],
    \['darkgreen', '#00FF00'],
    \['White', '#c0c0c0'],
    \['blue', '#FF6000'],
    \['cyan', '#00FFFF'],
    \['darkmagenta', '#CC00FF'],
    \['yellow', '#FFFF00'],
    \['red', '#FF0000'],
    \['darkgreen', '#00FF00'],
    \['White', '#c0c0c0'],
  \]

  augroup RainbowParentheses
    autocmd!
    autocmd VimEnter * RainbowParenthesesActivate
    autocmd BufEnter * RainbowParenthesesLoadRound
    autocmd BufEnter * RainbowParenthesesLoadSquare
  augroup END

Plugin 'scrooloose/nerdtree'
	autocmd StdinReadPre * let s:std_in=1
	autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
	let g:NERDTreeShowBookmarks = 1
        let NERDTreeShowHidden=1

Plugin 'scrooloose/syntastic'
  let g:syntastic_check_on_open=1
  let g:syntastic_mode_map={
    \'mode': 'active',
    \'active_filetypes': [],
    \'passive_filetypes': ['html', 'java']
  \}
Plugin 'nathanaelkane/vim-indent-guides'
	nnoremap <silent> <leader>i :IndentGuidesToggle<CR>
	let g:indent_guides_enable_on_vim_startup=1
	let g:indent_guides_auto_colors=0

	augroup IndentGuideColors
		autocmd!
		autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd ctermfg=white ctermbg=234
		autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermfg=white ctermbg=236
	augroup END

call vundle#end()
filetype plugin indent on
set backspace=2
let g:promptline_preset = {
  \'a' : [ promptline#slices#user() ],
  \'b' : [ promptline#slices#jobs(), promptline#slices#vcs_branch() ],
  \'c' : [ promptline#slices#cwd() ],
  \'warn' : [ promptline#slices#last_exit_code() ]
\}

let g:Gitv_OpenHorizontal = 1
let g:Gitv_TruncateCommitSubjects = 1
let g:Gitv_PromptToDeleteMergeBranch = 1

cabbrev gitv Gitv

set laststatus=2  					" Always show the statusline
set encoding=utf-8 					" Necessary to show Unicode glyphs

imap <C-Return> <CR><CR><C-o>k<Tab>

 			" If you don't need indentation... TextEdit!
		" 8 is too much for my eyes	 
set autoindent
set expandtab
set softtabstop=2
set shiftwidth=2

set autoread						" read alien changes automatically
set autowrite						" auto write file when thowing (e.g.) :make
set hidden							" they deserve backgrounding
"au BufAdd,BufNewFile,BufRead * nested tab sball " Love tabs, but it breaks! :(
set shortmess+=a 					" Short messages are short :)
set shortmess+=I 					" Forget about Vim's intro message
set nobackup						" Disable old-school backups, and
set noswapfile						" for good coder's sake use a repo :P
set mouse=a							" Sorry, I like my touchpad. Not nerd enough
set showmatch						" {}()[]
set number
set listchars=eol:$,tab:>-,trail:.,extends:>,precedes:<,nbsp:_
set whichwrap+=<,>,h,l,[,]			" Wrap navigation in normal and insert!
set clipboard=unnamed				" Yank will go to mac's clipboard
set formatoptions=tcrqnl 			" Pretty common setting +n
set wildmenu						" Autocompletion rulez
set wildignore+=*.a,*.o				" Leave out files from completion
set wildignore+=*.bmp,*.gif,*.ico,*.jpg,*.png
set wildignore+=*~,*.swp,*.tmp
set wildmode=list:longest,full
set completeopt=menu,longest,preview


syntax enable
let g:solarized_termcolors=256
set background=dark
colorscheme solarized				" they switch between Dark and Light
